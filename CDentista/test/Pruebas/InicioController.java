/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pruebas;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author VasMa
 */
public class InicioController implements Initializable {

    @FXML
    private AnchorPane vista;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void consultaInicio(ActionEvent event) throws IOException {
        Pane ini = FXMLLoader.load(getClass().getResource("/fileFXML/Consulta.fxml"));
        vista.getChildren().setAll(ini);
    }

    @FXML
    private void mensajeriaInicio(ActionEvent event) throws IOException {
        Pane ini = FXMLLoader.load(getClass().getResource("/fileFXML/Chat1.fxml"));
        vista.getChildren().setAll(ini);
    }

    @FXML
    private void agendaInicio(ActionEvent event) throws IOException {
        Pane ini = FXMLLoader.load(getClass().getResource("/fileFXML/Agenda1.fxml"));
        vista.getChildren().setAll(ini);
    }


    @FXML
    private void ayudaInicio(ActionEvent event) throws IOException {
        Pane ini = FXMLLoader.load(getClass().getResource("/fileFXML/Ayuda.fxml"));
        vista.getChildren().setAll(ini);
    }
    
}
