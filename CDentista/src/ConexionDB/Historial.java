/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConexionDB;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 *
 * @author Jose Pablo
 */
public class Historial {
 
    private IntegerProperty idPaciente;
    private IntegerProperty idDiente;
    private StringProperty Nombre;
    private StringProperty diente;
    private StringProperty posicion;
    private StringProperty estadoDiente;
    private StringProperty diaProcedimiento;
    private StringProperty descripcion;
    
    public Historial(int idPaciente, String Nombre,int idDiente,  String diente, String posicion, String estadoDiente, String diaProcedimiento, String descripcion){
        this.idPaciente = new SimpleIntegerProperty(idPaciente);
        this.idDiente = new SimpleIntegerProperty(idDiente);
        this.Nombre = new SimpleStringProperty(Nombre);
        this.diente = new SimpleStringProperty(diente);
        this.posicion = new SimpleStringProperty(posicion);
        this.estadoDiente = new SimpleStringProperty(estadoDiente);
        this.diaProcedimiento = new SimpleStringProperty(diaProcedimiento);
        this.descripcion = new SimpleStringProperty(descripcion);
    }

    public Integer getIdPaciente() {
        return idPaciente.get();
    }

    public void setIdPaciente(Integer idPaciente) {
        this.idPaciente = new SimpleIntegerProperty(idPaciente);
    }

    public Integer getIdDiente() {
        return idDiente.get();
    }

    public void setIdDiente(Integer idDiente) {
        this.idDiente = new SimpleIntegerProperty(idDiente);
    }

    public String getNombre() {
        return Nombre.get();
    }

    public void setNombre(String Nombre) {
        this.Nombre = new SimpleStringProperty(Nombre);
    }

    public String getDiente() {
        return diente.get();
    }

    public void setDiente(String diente) {
        this.diente = new SimpleStringProperty(diente);
    }

    public String getPosicion() {
        return posicion.get();
    }

    public void setPosicion(String posicion) {
        this.posicion = new SimpleStringProperty(posicion);
    }

    public String getEstadoDinte() {
        return estadoDiente.get();
    }

    public void setEstadoDinte(String estadoDiente) {
        this.estadoDiente = new SimpleStringProperty(estadoDiente);
    }

    public String getDiaProcedimiento() {
        return diaProcedimiento.get();
    }

    public void setDiaProcedimiento(String diaProcedimiento) {
        this.diaProcedimiento = new SimpleStringProperty(diaProcedimiento);
    }

    public String getDescripcion() {
        return descripcion.get();
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = new SimpleStringProperty(descripcion);
    }
    /**
    * Llena los datos que estan en la tabla de historial 
    * @see
    */
    public static void llenarTablaHistorial(Connection connection,ObservableList<Historial> lista, String nombre, String apellidoP,
    String apellidoM){
		try {
                    String sql="";
                    if(nombre.equals("") || apellidoP.equals("") || apellidoM.equals("")){
                        sql ="Select "
                                + "paciente.idpaciente, "
                                + "Concat(paciente.nombre, ' ', paciente.apellido_paterno, ' ', paciente.apellido_materno) as Nombre,"
                                + "historial.idDiente, "
                                + "diente.nombre, "
                                + "diente.posicion, "
                                + "historial.estadoDiente, "
                                + "historial.diaProcedimiento, "
                                + "historial.descripcion "
                                + "from bookmedic.paciente inner join bookmedic.historial 	"
                                + "on paciente.idpaciente = historial.idPaciente inner join bookmedic.diente  "
                                + "on diente.idDiente = historial.idDiente ";
                                
                    }else{
                            sql="Select "
                                    + "paciente.idpaciente, "
                                    + "Concat(paciente.nombre, ' ', paciente.apellido_paterno, ' ', paciente.apellido_materno) as Nombre,"
                                    + "historial.idDiente, "
                                    + "diente.nombre, "
                                    + "diente.posicion, "
                                    + "historial.estadoDiente, "
                                    + "historial.diaProcedimiento, "
                                    + "historial.descripcion "
                                    + "from bookmedic.paciente inner join bookmedic.historial 	on paciente.idpaciente = historial.idPaciente inner join bookmedic.diente  on diente.idDiente = historial.idDiente "
                                    + "where paciente.nombre = '"+ nombre +"' and paciente.apellido_paterno = '" + apellidoP +"' and apellido_materno ='" + apellidoM+"'";
                    }
			Statement st = connection.createStatement();
                        ResultSet resultado = st.executeQuery(sql);
                        //Statement instruccion = connection.createStatement();
			//ResultSet resultado = instruccion.executeQuery();
			while(resultado.next()){                            
				lista.add(
						new Historial(
								resultado.getInt("idpaciente"),
                                                                resultado.getString("Nombre"),        
                                                                resultado.getInt("idDiente"),
								resultado.getString("diente.nombre"),
                                                                resultado.getString("posicion"),
                                                                resultado.getString("estadoDiente"),
                                                                resultado.getString("diaProcedimiento"),
                                                                resultado.getString("descripcion")
                                                                
						)
				);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}