/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConexionDB;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 *
 * @author Jose Pablo
 */
public class Medicos {
    	private IntegerProperty idMedico;
	private StringProperty nombre;
        private StringProperty apellidoP;
        private StringProperty apellidoM;
        
        
    public Medicos(Integer idMedico, String nombre, String apellidoP, String apellidoM){
            this.idMedico = new SimpleIntegerProperty(idMedico);
            this.nombre = new SimpleStringProperty(nombre);
            this.apellidoP = new SimpleStringProperty(apellidoP);
            this.apellidoM = new SimpleStringProperty(apellidoM);
        }
   

    public Integer getIdMedico() {
        return idMedico.get();
    }

    public void setIdMedico(Integer idMedico) {
        this.idMedico = new SimpleIntegerProperty(idMedico);
    }

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre = new SimpleStringProperty(nombre);
    }

    public String getApellidoP() {
        return apellidoP.get();
    }

    public void setApellidoP(String apellidoP) {
        this.apellidoP = new SimpleStringProperty(apellidoP);
    }

    public String getApellidoM() {
        return apellidoM.get();
    }

    public void setApellidoM(String apellidoM) {
        this.apellidoM = new SimpleStringProperty(apellidoM);
    }
    /**
    * Llena el conbox de medicos con la informacion de la base de datos
    * @see
    */
    public static void llenarInformacion(Connection connection, ObservableList<Medicos> lista){
            String sql="";
        try {
                sql="Select idMedico, nombre, apellidoPaterno, apellidoMaterno from bookmedic.medico";
			Statement statement = connection.createStatement();
			ResultSet resultado = statement.executeQuery(sql);
			while (resultado.next()){
				lista.add(
						new Medicos(
								resultado.getInt("idMedico"),
								resultado.getString("nombre"),
                                                                resultado.getString("apellidoPaterno"),
                                                                resultado.getString("apellidoMaterno")                                                        
						)
				);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String toString(){
		return /*idMedico.getValue().toString();*/ ("("+idMedico.get()+")"+ " " + nombre.get() + " "+ apellidoP.get()+ " " + apellidoM.get());
	}
    
}
