/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConexionDB;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 *
 * @author Jose Pablo
 */
public class Agenda {
        private IntegerProperty id;
	private StringProperty nombre;
	private StringProperty apellidoP;
        private StringProperty apellidoM;
	private StringProperty fecha;
	private StringProperty hora;
	
        
        public Agenda(Integer id, String nombre, String apellidoP, String apellidoM, String fecha, String hora){
            this.id = new SimpleIntegerProperty(id);
            this.nombre = new SimpleStringProperty(nombre);
            this.apellidoP = new SimpleStringProperty(apellidoP);
            this.apellidoM = new SimpleStringProperty(apellidoM);
            this.fecha = new SimpleStringProperty(fecha);
            this.hora = new SimpleStringProperty(hora);
            
        }

    public Integer getId() {
        return id.get();
    }

    public void setId(Integer id) {
        this.id = new SimpleIntegerProperty(id);
    }

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre = new SimpleStringProperty(nombre);
    }

    public String getApellidoP() {
        return apellidoP.get();
    }

    public void setApellidoP(String apellidoP) {
        this.apellidoP = new SimpleStringProperty(apellidoP);
    }

    public String getApellidoM() {
        return apellidoM.get();
    }

    public void setApellidoM(String apellidoM) {
        this.apellidoM = new SimpleStringProperty(apellidoM);
    }

    public String getFecha() {
        return fecha.get();
    }

    public void setFecha(String fecha) {
        this.fecha =  new SimpleStringProperty(fecha);
    }

    public String getHora() {
        return hora.get();
    }

    public void setHora(String hora) {
        this.hora = new SimpleStringProperty(hora);
    }
    
   public static void llenarInformacionAlumnos(Connection connection,ObservableList<Agenda> lista, String nombre, String apellidoP,String apellidoM){
       String sql="";
		try {
                    
                    if(nombre.equals("")|| apellidoP.equals("") || apellidoM.equals("")){                                
                                sql="select "
                                + "citas.idpaciente, "
                                + "nombre, apellido_paterno, "
                                + "apellido_materno, "
                                + "fecha,"
                                        + "Asunto "
                                + "from bookmedic.citas inner join bookmedic.paciente "
                                + "on paciente.idpaciente = citas.idpaciente ";
                                //+ "where fecha = '" + Fecha()+"'";*/
                    }else{
                                
                                sql="Select "
                                + "citas.idPaciente, "
                                + "Nombre,"
                                + "apellido_paterno, "
                                + "apellido_materno, "
                                + "fecha,"
                                        + "Asunto "
                                + "from citas inner join paciente "
                                + "on citas.idpaciente = paciente.idpaciente "
                                + "where nombre ='"+nombre +"' and "
                                + "apellido_paterno= '"+apellidoP+"' and "
                                + "apellido_materno= '"+apellidoM+"'";
                                //+ "where fecha = '" + Fecha()+"'";*/                        
                    }                                                        
			Statement st = connection.createStatement();
                        ResultSet resultado = st.executeQuery(sql);                                                
			while(resultado.next()){                            
				lista.add(
						new Agenda(
								resultado.getInt("idPaciente"),
								resultado.getString("Nombre"),
								resultado.getString("apellido_paterno"),
                                                                resultado.getString("apellido_materno"),
                                                                resultado.getString("fecha"),
                                                                resultado.getString("Asunto")
						)
				);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
   /**
    * Obtiene la fecha local de la computadora
    * @see
    */
    public static String Fecha(){
         java.util.Date fecha = new java.util.Date();
         SimpleDateFormat formato = new SimpleDateFormat("dd/MM/YYYY");
         return formato.format(fecha);                  
        
     }
    
    /**
    *Llena la informacion de la agenda en la tabla de Agenda
    *@return h
    */
    public static void llenarInformacionAlumnos1(Connection connection,ObservableList<Agenda> lista){
		try {
                    String sql="";
                    
                    sql="select "
                    + "citas.idpaciente, "
                    + "nombre, apellido_paterno, "
                    + "apellido_materno, "
                    + "fecha,"
                            + "Asunto "
                    + "from bookmedic.citas inner join bookmedic.paciente "
                    + "on paciente.idpaciente = citas.idpaciente ";
                    //+ "where fecha = '" + Fecha()+"'";*/
			Statement st = connection.createStatement();
                        ResultSet resultado = st.executeQuery(sql);
                        //Statement instruccion = connection.createStatement();
			//ResultSet resultado = instruccion.executeQuery();
			while(resultado.next()){                            
				lista.add(
						new Agenda(
								resultado.getInt("idPaciente"),
								resultado.getString("Nombre"),
								resultado.getString("apellido_paterno"),
                                                                resultado.getString("apellido_materno"),
                                                                resultado.getString("fecha"),
                                                                resultado.getString("Asunto")
						)
				);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
    
}
