/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import ConexionDB.Conexion;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.VBox;


/**
 * FXML Controller class
 *
 * @author SYSTEM
 */
public class PacientesController implements Initializable {

    @FXML
    private VBox pnl_scroll;
    @FXML
    private JFXTextField txtNombre;
    @FXML
    private JFXTextField txtApellido;
    
    private Conexion cc = new Conexion();
    private Connection cn= cc.conexion();
    private int idMedico;
    HomeController control;
    static PacientesController controlador;
    ItemConexion item;
    String apellidoM;
    String apellidoP;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    
    public void setIdMedico(int idMedico) throws IOException{
        this.idMedico = idMedico;
        Pacientes(1);
    }
    
    
    
    public int Pacientes(int num) throws IOException{
        System.out.println(idMedico);
        int r = 0;
        pnl_scroll.getChildren().clear();
        Node [] nodes = new  Node[1000];
        int i=0;
        String sql = null;
        if(num==1){
            sql = "SELECT * FROM paciente WHERE idMedico = "+ idMedico +"";
       }else{
           sql = "SELECT * FROM paciente WHERE nombre Like '"+ txtNombre.getText() +"%' and apellido_paterno like '"+ txtApellido.getText() + "%'";
       }
        try {
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql); 
                while(rs.next()){
                    int id =rs.getInt(1);
                    String nombre = rs.getString(2) + " " + rs.getString(3) + " "+ rs.getString(4);
                    String nacimiento = rs.getString(5);
                    String domicilio = rs.getString(6)+ " " + rs.getString(7) + " " + rs.getString(8);
                    String telefono = rs.getString(9);
                    String email = rs.getString(10);
                    
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/FileFXML/Item.fxml"));
                    Object carga = loader.load();
                    nodes[i] = (Node)carga;
                    Parent sceneMain = (Parent) carga;
                    ItemController controller = loader.<ItemController>getController();
                    controller.setInformacion(id, nombre, nacimiento);
                    controller.setController(controlador);
                    controller.setClass(item);
                    controller.setInformacionContacto(telefono, email);
                    controller.setDomicilio(domicilio);
                    controller.llenarInformacion();
                    pnl_scroll.getChildren().add(nodes[i]);
                    
                    i++;
                }
            } catch (SQLException e) {
                Logger.getLogger(Loginv2Controller.class.getName()).log(Level.SEVERE, null, e);
            }
        return r;
    }
    
    public void setController(HomeController controller){
        control = controller;
    }
    
    

    @FXML
    private void buscar(ActionEvent event) throws IOException {
        Pacientes(2);
    }
    
    public void paciente(PacientesController contro){
        controlador = contro;
    }
    
    public void setClass(ItemConexion item){
        this.item= item;
    }
}
