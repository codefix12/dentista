/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author SYSTEM
 */
public class ItemController implements Initializable {

    @FXML
    private Label nombre;
    @FXML
    private Label telefono;
    @FXML
    private Label email;
    @FXML
    private Label dientes;
    @FXML
    private Label idPaciente;
    @FXML
    private Label dia;
    @FXML
    private Label mes;
    @FXML
    private Label año;
    @FXML
    private Label domicilio;
    private int id;
    private String name;
    private String phone;
    private String correo;
    private String diente;
    private String date;
    private String domi;
    static PacientesController controlador;
    ItemConexion item;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    private void infoDental(ActionEvent event) {
    }

    @FXML
    private void consulta(ActionEvent event) throws IOException {
        item.openConsulta();
    }

    @FXML
    private void infoGeneral(ActionEvent event) {
    }
    
    public void setInformacion(int idPaciente, String nombre, String nacimiento){
        id = idPaciente;
        name = nombre;
        date = nacimiento;
    }
    
    public void setInformacionContacto(String tel, String email ){
        phone = tel;
        correo = email;
    }
    
    public void setDomicilio(String domicilio){
        domi=domicilio;
    }
    
    public void llenarInformacion(){
        idPaciente.setText("ID:"+String.valueOf(id));
        nombre.setText(name);
        if(phone != null){
            telefono.setText(phone);
        }
        if(correo != null){
            email.setText(correo);
        }
        fechaNacimiento();
        domicilio.setText(domi);
    }
    
    private void fechaNacimiento(){
        String[] parts = date.split("-");
        String anio = parts[0]; // 123
        String month = parts[1];
        String day = parts[2];
        switch(month){
            case "01":
                mes.setText("Ene");
                break;
            case "02":
                mes.setText("Feb");
                break;
            case "03":
                mes.setText("Mar");
                break;
            case "04":
                mes.setText("Abr");
                break;    
            case "05":
                mes.setText("May");
                break;
            case "06":
                mes.setText("Jun");
                break;
            case "07":
                mes.setText("Jul");
                break;
            case "08":
                mes.setText("Ago");
                break;
            case "09":
                mes.setText("Sep");
                break;
            case "10":
                mes.setText("Oct");
                break;
            case "11":
                mes.setText("Nov");
                break;
            case "12":
                mes.setText("Dic");
                break;
        }
        año.setText(anio);
        dia.setText(day);
    }
    
    public void setController(PacientesController controller){
        controlador = controller;
    }
    
     public void setClass(ItemConexion item){
        this.item= item;
    }
}
