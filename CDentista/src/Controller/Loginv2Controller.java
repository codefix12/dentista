/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import ConexionDB.Conexion;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.awt.HeadlessException;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * FXML Controller class
 *
 * @author SYSTEM
 */
public class Loginv2Controller implements Initializable {

    @FXML
    private Hyperlink jHyperLink1;
    @FXML
    private JFXButton botonCerrar;
    @FXML
    private JFXButton botonMinimizar;
    @FXML
    private AnchorPane registro1;
    @FXML
    private Label errorRegistro;
    @FXML
    private AnchorPane registro2;
    @FXML
    private AnchorPane login;
    @FXML
    private JFXTextField usuario;
    @FXML
    private JFXPasswordField contrasena;
    @FXML
    private JFXTextField registroUsuario;
    @FXML
    private JFXPasswordField registroContrasena;
    @FXML
    private JFXPasswordField repiteContrasena;
    @FXML
    private JFXTextField nombre;
    @FXML
    private JFXTextField apellidoP;
    @FXML
    private JFXTextField apellidoM;
    @FXML
    private JFXDatePicker date;
    @FXML
    private AnchorPane registro3;
    @FXML
    private JFXTextField especialista;
    @FXML
    private JFXTextField cedProf;
    @FXML
    private JFXTextField telefono;
    @FXML
    private JFXTextField email;
    private Conexion cc = new Conexion();
    private Connection cn= cc.conexion();
    private String usuario1;
    private String pass;
    private String name;
    private String last1;
    private String last2;
    private String day;
    private String especialist;
    private String cedPro;
    private String tel;
    private String correo;
    private String userDB;
    @FXML
    private Label errorContraseña;
    private int idPaciente;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        incremento();
    }    
    
    /*EVENTOS EN LA RAIZ PRINCIPAL*/
    
    @FXML
    private void close(ActionEvent event) {
        System.exit(0);
    }
    
    @FXML
    private void menuLogin(ActionEvent event) {
        botonCerrar.setLayoutX(353);
        botonMinimizar.setLayoutX(314);
        login.setVisible(true);
        registro2.setVisible(false);
        registro1.setVisible(false);
        registro3.setVisible(false);
        limpiarRegistro();
    }

    @FXML
    private void menuRegistro(ActionEvent event) {
        botonCerrar.setLayoutX(385);
        botonMinimizar.setLayoutX(346);
        registro1.setVisible(true);
        registro2.setVisible(true);
        registro3.setVisible(true);
        login.setVisible(false);
        registro1.toFront();
        usuario.setText("");
        contrasena.setText("");
    }
    
    @FXML
    private void minimizar(ActionEvent event) {
    }
    
    /*TERMINA LA RAIZ PRINCIPAL*/
    

    
    /*EVENTOS DEL LOGIN*/
    
    @FXML
    private void registroUser(MouseEvent event) {
        registro1.setVisible(true);
        registro2.setVisible(true);
        registro3.setVisible(true);
        login.setVisible(false);
        registro1.toFront();
        botonCerrar.setLayoutX(385);
        botonMinimizar.setLayoutX(346);
    }

    @FXML
    private void ingresarEnter(KeyEvent event) throws IOException {
        if(event.getCode().toString().equals("ENTER")){
            if(contrasena.getText().equals("") || usuario.getText().equals("")){
                Alert dialogAlert2 = new Alert(Alert.AlertType.WARNING);
                dialogAlert2.setTitle("Advertencia");
                dialogAlert2.setContentText("Hay Campos Vacios");
                dialogAlert2.initStyle(StageStyle.UTILITY);
                dialogAlert2.showAndWait();
            }else{
                logear(usuario.getText().toLowerCase());
            }
        }
    }

    @FXML
    private void iniciarSesion(MouseEvent event) throws IOException {
        if(contrasena.getText().equals("") || usuario.getText().equals("")){
                Alert dialogAlert2 = new Alert(Alert.AlertType.WARNING);
                dialogAlert2.setTitle("Advertencia");
                dialogAlert2.setContentText("Hay Campos Vacios");
                dialogAlert2.initStyle(StageStyle.UTILITY);
                dialogAlert2.showAndWait();
            }else{
                logear(usuario.getText().toLowerCase());
            }
    }

    int id;
    String name1;
    
    /**
    * Acredita que el usuario existe
    * @param username nombre de usuario del medico
    */
    public void logear(String username) throws IOException{
        String pass = contrasena.getText().toLowerCase();
        pass= DigestUtils.md5Hex(pass);
        String passDb = null;
        String sql = "SELECT * FROM medico WHERE usuario = '"+ username +"'";
        try {
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql); 
                while(rs.next()){
                    id=rs.getInt(1);
                    name1 = rs.getString(4) + " " + rs.getString(5) + " " + rs.getString(6);
                    userDB = rs.getString(2);
                    //userDb=rs.getString(2);
                    passDb=rs.getString(3);
                }
            } catch (SQLException e) {
                Logger.getLogger(Loginv2Controller.class.getName()).log(Level.SEVERE, null, e);
            }
        if(pass.equals(passDb)){
            Stage stage = new Stage();
            FXMLLoader loader= new FXMLLoader(getClass().getResource("/FileFXML/Home.fxml"));
            Object carga = loader.load();
            Parent root = (Parent) carga;
            Scene scene = new Scene(root);
            HomeController controller = loader.<HomeController>getController();
            //HomeController d = (HomeController)loader.getController();
            controller.setInformationDentista(id, name, userDB);
            controller.setController(controller);
            stage.getIcons().add(new Image(Loginv2Controller.class.getResourceAsStream( "/NCSS/Login.png" ))); 
            stage.setScene(scene);
            stage.alwaysOnTopProperty();
            stage.initModality(Modality.APPLICATION_MODAL);
           
            stage.show();
            Stage stage1 = (Stage) jHyperLink1.getScene().getWindow();
            stage1.close();
            
        }else{
            Alert dialogAlert2 = new Alert(Alert.AlertType.WARNING);
            dialogAlert2.setTitle("Advertencia");
            dialogAlert2.setContentText("Contraseña Incorrecta");
            dialogAlert2.initStyle(StageStyle.UTILITY);
            dialogAlert2.showAndWait();
        }
            
    }
    /*TERMINA EL LOGIN*/

    
    
    
    /*EVENTOS DEL REGISTRO*/
    

    @FXML
    private void siguienteGral(ActionEvent event) {
        errorRegistro.setVisible(false);
        errorContraseña.setVisible(false);
        if(repiteContrasena.getText().equalsIgnoreCase("") || registroUsuario.getText().equalsIgnoreCase("") || registroContrasena.getText().equalsIgnoreCase("")){
            Alert dialogAlert2 = new Alert(Alert.AlertType.WARNING);
            dialogAlert2.setTitle("Advertencia");
            dialogAlert2.setContentText("Hay Campos Vacios");
            dialogAlert2.initStyle(StageStyle.UTILITY);
            dialogAlert2.showAndWait();
        }else{
            usuario1=registroUsuario.getText().toLowerCase();
            if(busqueda(usuario1)){
                
                if(registroContrasena.getText().equalsIgnoreCase(repiteContrasena.getText().toLowerCase())){
                    String pass1=registroContrasena.getText().toLowerCase();
                    pass = DigestUtils.md5Hex(pass1); 
                    registro2.toFront();
                }else{
                    errorContraseña.setVisible(true);
                }
            }else{
                errorRegistro.setVisible(true);
            }
            
        }
    }
    
    @FXML
    private void siguienteContacto(ActionEvent event) {
        day = date.getValue().toString();
        //System.out.println(dia);
        if(nombre.getText().equalsIgnoreCase("") || apellidoP.getText().equalsIgnoreCase("") || 
                apellidoM.getText().equalsIgnoreCase("") || day.equals("")){
            Alert dialogAlert2 = new Alert(Alert.AlertType.WARNING);
            dialogAlert2.setTitle("Advertencia");
            dialogAlert2.setContentText("Hay Campos Vacios");
            dialogAlert2.initStyle(StageStyle.UTILITY);
            dialogAlert2.showAndWait();
        }else{
            name = nombre.getText();
            last1= apellidoP.getText();
            last2= apellidoM.getText();
            registro3.toFront();
        }
    }

    @FXML
    private void regresarUsuario(ActionEvent event) {
        errorRegistro.setVisible(false);
        errorContraseña.setVisible(false);
        registro1.toFront();
    }

    @FXML
    private void registrar(ActionEvent event) {
        if(especialista.getText().equals("") || cedProf.getText().equals("")){
            Alert dialogAlert2 = new Alert(Alert.AlertType.WARNING);
            dialogAlert2.setTitle("Advertencia");
            dialogAlert2.setContentText("El campo de Especialista o Cedula Profesional esta vacio");
            dialogAlert2.initStyle(StageStyle.UTILITY);
            dialogAlert2.showAndWait();
        }else{
            especialist = especialista.getText();
            cedPro = cedProf.getText();
            tel = telefono.getText();
            correo = email.getText();
            if(registro()){
                limpiarRegistro();
                Alert dialogAlert2 = new Alert(Alert.AlertType.INFORMATION);
                dialogAlert2.setTitle("Registrado");
                dialogAlert2.setContentText("Se ha resgistrado satisfactoriamente");
                dialogAlert2.initStyle(StageStyle.UTILITY);
                dialogAlert2.showAndWait();
                registro1.setVisible(false);
                registro2.setVisible(false);
                registro3.setVisible(false);
                login.setVisible(true);
                botonCerrar.setLayoutX(353);
                botonMinimizar.setLayoutX(314);
            }else{
            Alert dialogAlert2 = new Alert(Alert.AlertType.WARNING);
            dialogAlert2.setTitle("Advertencia");
            dialogAlert2.setContentText("Algo fallo, vuelve a intentarlo");
            dialogAlert2.initStyle(StageStyle.UTILITY);
            dialogAlert2.showAndWait();
            }
        }
        
    }

    @FXML
    private void regresarGral(ActionEvent event) {
        registro2.toFront();
    }
    

    /**
    * Realiza una busqueda dentro de la base de datos de manera que el usuario
    * solicitado no exista dentro de ella
    * @param user nombre del usuario
    */
    public boolean busqueda(String user){
       String user1 = null;
        String sql = "SELECT * FROM medico WHERE usuario = '"+ user+"'";
        try {
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql); 
                while(rs.next()){
                    user1=rs.getString(2);
                }
            } catch (SQLException e) {
                Logger.getLogger(Loginv2Controller.class.getName()).log(Level.SEVERE, null, e);
                    
            }
        if(user.equalsIgnoreCase(user1)){
            return false;
        }else{
            return true;
        }
    }
    boolean acceso;
    /**
    * Realiza el registro del medico en la base de datos
    * Devuelve true si ha sido registrado o false si hubo problemas
    * @return acceso
    */
    public boolean registro(){
        
        try{
        PreparedStatement pst = cn.prepareStatement("INSERT INTO medico(idMedico, usuario, contraseña, nombre, apellidoPaterno, apellidoMaterno, fechaNacimiento, especialidad, cedulaProf, Telefono, email) VALUES (?,?,?,?,?,?,?,?,?,?,?)");        
            pst.setInt(1, idPaciente);
            pst.setString(2, usuario1);
            pst.setString(3, pass);
            pst.setString(4, name);
            pst.setString(5, last1);        
            pst.setString(6, last2);
            pst.setString(7, day);
            pst.setString(8, especialist);
            pst.setString(9, cedPro);
            pst.setString(10, tel);
            pst.setString(11, correo);
            pst.executeUpdate();
            acceso = true;
        }catch(HeadlessException | SQLException e){
            acceso=false;
            Logger.getLogger(Loginv2Controller.class.getName()).log(Level.SEVERE, null, e);
        }
        return acceso;
    }
    
    public void limpiarRegistro(){
        registroUsuario.setText("");
        registroContrasena.setText("");
        repiteContrasena.setText("");
        nombre.setText("");
        apellidoP.setText("");
        apellidoM.setText("");
        //date=null;
        especialista.setText("");
        cedProf.setText("");
        telefono.setText("");
        email.setText("");
    }
    private void incremento(){
        try {
            String sql = "select max(idMedico) from medico";
            idPaciente = 0;
            Statement st = cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                idPaciente = rs.getInt(1)+1;
            }
        } catch (SQLException ex) {
            Logger.getLogger(MenuDientev2Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
