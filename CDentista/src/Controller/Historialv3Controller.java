/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import ConexionDB.Agenda;
import ConexionDB.Conection;
import ConexionDB.Historial;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author Jose Pablo
 */
public class Historialv3Controller implements Initializable {

    @FXML    private TableView<Historial> tblHistorial;
    @FXML    private JFXTextField txtNombre;
    @FXML    private JFXTextField txtAepllidoPaterno;
    @FXML    private JFXTextField txtApellidoMaterno;
    @FXML    private TableColumn<Historial, Number> idPaciente;
    @FXML    private TableColumn<Historial, String> nombre;
    @FXML    private TableColumn<Historial, Number> idDiente;
    @FXML    private TableColumn<Historial, String> diente;
    @FXML    private TableColumn<Historial, String> posicion;
    @FXML    private TableColumn<Historial, String> estadoDiente;
    @FXML    private TableColumn<Historial, String> diaProcedimineto;
    @FXML    private TableColumn<Historial, String> descripcion;
     private ObservableList<Historial> historial;    
    private Conection conexion;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        conexion = new Conection();
	conexion.establecerConexion();
        historial = FXCollections.observableArrayList();        
        Historial.llenarTablaHistorial(conexion.getConnection(), historial, txtNombre.getText(), txtAepllidoPaterno.getText(), txtApellidoMaterno.getText());
        tblHistorial.setItems(historial);
        
        idPaciente.setCellValueFactory(new PropertyValueFactory<Historial,Number>("idPaciente"));        
        nombre.setCellValueFactory(new PropertyValueFactory<Historial, String>("nombre"));
        idDiente.setCellValueFactory(new PropertyValueFactory<Historial,Number>("idDiente"));
        diente.setCellValueFactory(new PropertyValueFactory<Historial, String>("diente"));
        posicion.setCellValueFactory(new PropertyValueFactory<Historial, String>("posicion"));
        estadoDiente.setCellValueFactory(new PropertyValueFactory<Historial, String>("estadoDinte"));
        diaProcedimineto.setCellValueFactory(new PropertyValueFactory<Historial,String>("diaProcedimiento"));
        descripcion.setCellValueFactory(new PropertyValueFactory<Historial,String>("descripcion"));
        
        conexion.cerrarConexion();
    }    

    @FXML
    private void BuscarHistorial(ActionEvent event) {
        if(txtNombre.getText().equals("") || txtAepllidoPaterno.getText().equals("") ||txtApellidoMaterno.getText().equals("")){
            Alert dialogAlert2 = new Alert(Alert.AlertType.WARNING);
            dialogAlert2.setTitle("Advertencia");
            dialogAlert2.setContentText("Hay Campos Vacios");
            dialogAlert2.initStyle(StageStyle.UTILITY);
            dialogAlert2.showAndWait();            
        }else{
            conexion = new Conection();
	conexion.establecerConexion();
        historial = FXCollections.observableArrayList();        
        Historial.llenarTablaHistorial(conexion.getConnection(), historial, txtNombre.getText(), txtAepllidoPaterno.getText(), txtApellidoMaterno.getText());
        tblHistorial.setItems(historial);
        
        idPaciente.setCellValueFactory(new PropertyValueFactory<Historial,Number>("idPaciente"));        
        nombre.setCellValueFactory(new PropertyValueFactory<Historial, String>("nombre"));
        idDiente.setCellValueFactory(new PropertyValueFactory<Historial,Number>("idDiente"));
        diente.setCellValueFactory(new PropertyValueFactory<Historial, String>("diente"));
        posicion.setCellValueFactory(new PropertyValueFactory<Historial, String>("posicion"));
        estadoDiente.setCellValueFactory(new PropertyValueFactory<Historial, String>("estadoDinte"));
        diaProcedimineto.setCellValueFactory(new PropertyValueFactory<Historial,String>("diaProcedimiento"));
        descripcion.setCellValueFactory(new PropertyValueFactory<Historial,String>("descripcion"));
        
        conexion.cerrarConexion();
        }
    }
    
}
