/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import ConexionDB.Agenda;
import ConexionDB.Conection;
import ConexionDB.Conexion;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableView;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.StageStyle;
import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author SYSTEM
 */
public class Agendav3Controller implements Initializable {
       
    @FXML    private JFXDatePicker fechaBusqueda;
    @FXML    private JFXTextField nombreBusqueda;
    @FXML    private Label diaDeHoy;
    private ObservableList<Agenda> agenda;    
    private Conection conexion;
    @FXML    private TableView<Agenda> tblAgenda;
    //Columnas
    @FXML private TableColumn<Agenda,Number> id_Paciente;
    @FXML private TableColumn<Agenda,String> nombre;
    @FXML private TableColumn<Agenda,String> apellidoP;
    @FXML private TableColumn<Agenda,String> apellidoM;
    @FXML private TableColumn<Agenda,String> fecha;
    @FXML private TableColumn<Agenda,String> hora;
    @FXML
    private JFXTextField apellidoPBusqueda;
    @FXML
    private JFXTextField apellidoMBusqueda;
                
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO         
        
        conexion = new Conection();
	conexion.establecerConexion();
        
        agenda = FXCollections.observableArrayList();
        Agenda.llenarInformacionAlumnos(conexion.getConnection(), agenda, nombreBusqueda.getText(), apellidoPBusqueda.getText(), apellidoMBusqueda.getText());
        tblAgenda.setItems(agenda);
        
        id_Paciente.setCellValueFactory(new PropertyValueFactory<Agenda,Number>("id"));
        nombre.setCellValueFactory(new PropertyValueFactory<Agenda, String>("nombre"));
        apellidoP.setCellValueFactory(new PropertyValueFactory<Agenda, String>("apellidoP"));
        apellidoM.setCellValueFactory(new PropertyValueFactory<Agenda, String>("apellidoM"));
        fecha.setCellValueFactory(new PropertyValueFactory<Agenda, String>("fecha"));
        hora.setCellValueFactory(new PropertyValueFactory<Agenda, String>("hora"));
        
        conexion.cerrarConexion();
    }    
    
    @FXML
    private void Busqueda(ActionEvent event) {
        if(nombreBusqueda.getText().equals("") || apellidoPBusqueda.getText().equals("") || apellidoMBusqueda.getText().equals("")){
             Alert dialogAlert2 = new Alert(Alert.AlertType.WARNING);
            dialogAlert2.setTitle("Advertencia");
            dialogAlert2.setContentText("Hay Campos Vacios");
            dialogAlert2.initStyle(StageStyle.UTILITY);
            dialogAlert2.showAndWait();
        }else{
            conexion = new Conection();
	conexion.establecerConexion();
        
        agenda = FXCollections.observableArrayList();
        Agenda.llenarInformacionAlumnos(conexion.getConnection(), agenda, nombreBusqueda.getText(), apellidoPBusqueda.getText(), apellidoMBusqueda.getText());
        tblAgenda.setItems(agenda);
        
        id_Paciente.setCellValueFactory(new PropertyValueFactory<Agenda,Number>("id"));
        nombre.setCellValueFactory(new PropertyValueFactory<Agenda, String>("nombre"));
        apellidoP.setCellValueFactory(new PropertyValueFactory<Agenda, String>("apellidoP"));
        apellidoM.setCellValueFactory(new PropertyValueFactory<Agenda, String>("apellidoM"));
        fecha.setCellValueFactory(new PropertyValueFactory<Agenda, String>("fecha"));
        hora.setCellValueFactory(new PropertyValueFactory<Agenda, String>("hora"));
        
        conexion.cerrarConexion();
        }
        
    }            

    @FXML
    private void Busquedaporfecha(ActionEvent event) {
    }
    
    
}
