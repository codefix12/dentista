/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.IOException;

/**
 *
 * @author Isaac A.V. Marín
 */
public class ItemConexion {
    
    HomeController controlador;
    
    public void setController(HomeController controller){
        controlador= controller;
    }
    
    public void openConsulta() throws IOException{
        controlador.open();
    }
}
