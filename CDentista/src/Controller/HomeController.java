/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author SYSTEM
 */
public class HomeController implements Initializable {

    @FXML
    private Label nombreDentista;
    @FXML
    private Label especialidad;
    @FXML
    private AnchorPane vista;
    
    private String nombre;
    private String user;
    private int id;
    HomeController controlador;
    FXMLLoader loade;
    ItemConexion item = new ItemConexion();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Pane ini = null;
        try {
            ini = FXMLLoader.load(getClass().getResource("/FileFXML/Iniciov2.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        vista.getChildren().setAll(ini);
    }    

    @FXML
    private void logOut(ActionEvent event) throws IOException {
        Alert dialogAlert2 = new Alert(Alert.AlertType.CONFIRMATION);
        dialogAlert2.setTitle("Confirmacion");
        dialogAlert2.setContentText("");
        dialogAlert2.initStyle(StageStyle.UTILITY);
        dialogAlert2.showAndWait();
        String s = dialogAlert2.getResult().getText();
        //System.out.println(s);
        if(s.equals("Aceptar")){
            Stage stage = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("/FileFXML/Loginv2.fxml"));
            stage.initStyle(StageStyle.TRANSPARENT);
            Scene scene = new Scene(root);
            scene.setFill(Color.TRANSPARENT);
            stage.setScene(scene);
            stage.getIcons().add(new Image(HomeController.class.getResourceAsStream( "/NCSS/Login.png" ))); 
        //stage.setTitle("Iniciar Sesión");
            stage.show();
            
            Stage stage1 = (Stage) vista.getScene().getWindow();
            stage1.close();
        }
    }

    @FXML
    private void editarPerfil(ActionEvent event) {
    }

    @FXML
    private void botonInicio(ActionEvent event) throws IOException {
        Pane ini = FXMLLoader.load(getClass().getResource("/FileFXML/Iniciov2.fxml"));
        vista.getChildren().setAll(ini);
    }

    @FXML
    private void botonConfig(ActionEvent event) throws IOException {
        Pane ini = FXMLLoader.load(getClass().getResource("/FileFXML/About.fxml"));
        vista.getChildren().setAll(ini);
    }

    @FXML
    private void botonAyuda(ActionEvent event) throws IOException {
        Pane ini = FXMLLoader.load(getClass().getResource("/FileFXML/Ayudav2.fxml"));
        vista.getChildren().setAll(ini);
    }

    @FXML
    private void IniciarConsulta(ActionEvent event) throws IOException {
        open();
    }

    @FXML
    private void agenda(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/FileFXML/Agendav3.fxml"));
        Object carga = loader.load();
        Pane ini = (Pane) carga;
        Agendav3Controller controller = loader.<Agendav3Controller>getController();
        vista.getChildren().setAll(ini);
    }

    @FXML
    private void historialDental(ActionEvent event) throws IOException {
        Pane ini = FXMLLoader.load(getClass().getResource ("/FileFXML/Historial.fxml"));
        vista.getChildren().setAll(ini);
    }

    @FXML
    private void misPacientes(ActionEvent event) throws IOException {
        
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/FileFXML/Pacientes.fxml"));
        Object carga = loader.load();
        Pane ini = (Pane) carga;
        PacientesController controller = loader.<PacientesController>getController();
        controller.setIdMedico(id);
        controller.setController(controlador);
        item.setController(controlador);
        controller.setClass(item);
        vista.getChildren().setAll(ini);
        
    }
    
    /**
    * Ingresa al menú principal los datos del medico
    * @param id numero de id
    * @param nombre nombre del medico
    * @param user nombre del usuario
    */
    public void setInformationDentista(int id, String nombre, String user){
        this.id = id;
        this.nombre = nombre;
        this.user = user;
        nombreDentista.setText(user);
    }

    @FXML
    private void botonRegistro(ActionEvent event) throws IOException {
        System.out.println("Holi: "+id);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/FileFXML/RegistroPacientes.fxml"));
        Object carga = loader.load();
        Pane ini = (Pane) carga;
        RegistroPacienteController controller = loader.<RegistroPacienteController>getController();
        controller.setIdMedico(id);
        vista.getChildren().setAll(ini);
    }
    
    public void setController(HomeController controller){
        controlador= controller;
    }
    
    public void setFXController(FXMLLoader loader){
        loade= loader;
    }
    
    /**
    * Inicia la ventana de consulta
    * @see sx
    */
    public void open() throws IOException{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/FileFXML/Consultav2.fxml"));
        Object carga = loader.load();
        Pane ini = (Pane) carga;
        Consultav2Controller controller = loader.<Consultav2Controller>getController();
        controller.setController(controller);
        vista.getChildren().setAll(ini);
    }
    
    public void abrirConsultaPaciente(int id, String name, String fecha) throws IOException{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/FileFXML/Consultav2.fxml"));
        Object carga = loader.load();
        Pane ini = (Pane) carga;
        Consultav2Controller controller = loader.<Consultav2Controller>getController();
        controller.setController(controller);
        vista.getChildren().setAll(ini);
    }

    private void agendarCita(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/FileFXML/Agendar_consulta.fxml"));
        Object carga = loader.load();
        Pane ini = (Pane) carga;
        Agendar_citaController controller = loader.<Agendar_citaController>getController();
        vista.getChildren().setAll(ini);
    }

    @FXML
    private void agendarConsulta(ActionEvent event) throws IOException {
        Pane ini = FXMLLoader.load(getClass().getResource("/FileFXML/Agendar_consulta.fxml"));
        vista.getChildren().setAll(ini);
    }

    
    /**
    * Inicia el menu de carga de dientes
    * @param dato nombre del diente
    * @param id numero de id del paciente
    */
    private void buscarPacientes(ActionEvent event) throws IOException {
        Pane ini = FXMLLoader.load(getClass().getResource("/FileFXML/BusquedaDePacientes.fxml"));
        vista.getChildren().setAll(ini);
    }
}
