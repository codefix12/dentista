/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;

/**
 * FXML Controller class
 *
 * @author SYSTEM
 */
public class Ayudav2Controller implements Initializable {

    @FXML
    private TextArea Ayuda;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void ayudaConsulta(ActionEvent event) {
        Ayuda.setText("Preguntas Frecuentes:\n\n¿Qué hacer si no encuentro a mi paciente?"+
                "\nVerifica que no hay espacios en los campos de texto");
    }

    @FXML
    private void ayudaChat(ActionEvent event) {
        Ayuda.setText("");
        Ayuda.setText("Preguntas Frecuentes:\n\nNo se envian los mensajes"+
                "\nVerifica las si hay conexiones");
    }

    @FXML
    private void ayudaBuscar(ActionEvent event) {
        Ayuda.setText("");
    }

    @FXML
    private void ayudaAgenda(ActionEvent event) {
        Ayuda.setText("");
    }
    
}
