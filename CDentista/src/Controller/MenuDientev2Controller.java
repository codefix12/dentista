/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import ConexionDB.Conexion;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.awt.HeadlessException;
import java.net.URL;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author SYSTEM
 */
public class MenuDientev2Controller implements Initializable {

    @FXML
    private JFXComboBox<?> edoDiente;
    @FXML
    private JFXDatePicker fecha;
    @FXML
    private JFXTextField nombreDiente;
    @FXML
    private JFXTextArea descripcion;
    private Conexion cc = new Conexion();
    private Connection cn= cc.conexion();
    private int idDiente;
    private int idPaciente;
    private int idn;
    private boolean acreditar;
    private Consultav2Controller controlador;
    private int id;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        ObservableList oo=FXCollections.observableArrayList("Normal","Inexistente","En tratamiento", "Extraido");
        edoDiente.getItems().addAll(oo);
        //System.out.println(idPaciente + "dienre:" + idDiente);
        incremento();
        
    }    

    @FXML
    private void cargaProcedimiento(MouseEvent event) {
        String eleccion =edoDiente.getSelectionModel().getSelectedItem().toString();
        //System.out.println(eleccion);
        String date = fecha.getValue().toString();
        String detalles = descripcion.getText();
        cargarTratamiento(eleccion, date, detalles);
        //System.out.println(idPaciente + " diente: " + idDiente);
        
    }

    @FXML
    private void Cancelar(MouseEvent event) {
        Stage stage1 = (Stage) descripcion.getScene().getWindow();
        stage1.close();
    }
    
    
    /**
    *Inserta los datos del diente como su nombre e id
    *@param nombre ingresa los datos del diente
    *@param id numero de id del diente
    * @param idP id paciente
    */
    public void setDiente(String nombre, int id,int idP){
        nombreDiente.setText(nombre);
        idDiente = id ;
        existeTratamiento(idP,idDiente);
    }
    
    /**
    *Ingresa el id del paciente del tratamiento
    *@param id numero de id del paciente
    */
    public void setIDPaciente(int id){
        idPaciente = id;
    }
    int idTratamiento;//Auto incremento
    boolean acceso;
    /**
    *Ingresa el id del paciente del tratamiento
    *@param choice numero de id del paciente
    * @param fecha
    * @param description
    */
    private void cargarTratamiento(String choice, String fecha, String description){
        if(existeTratamiento(idPaciente,idDiente) == true){
            actualizar(choice,fecha,description);
        }else{
            try{
                PreparedStatement pst = cn.prepareStatement("INSERT INTO historial(idHistorial, idPaciente, idDiente, diaProcedimiento, estadoDiente, descripcion) VALUES (?,?,?,?,?,?)");        
                pst.setInt(1, idTratamiento);
                pst.setInt(2, idPaciente);
                pst.setInt(3, idDiente);
                pst.setString(4, fecha);
                pst.setString(5, choice);
                pst.setString(6, description);
                pst.executeUpdate();
                acceso = true;
                Alert dialogAlert2 = new Alert(Alert.AlertType.INFORMATION);
                dialogAlert2.setTitle("Hecho");
                dialogAlert2.setContentText("Tratamiento en curso");
                dialogAlert2.initStyle(StageStyle.UTILITY);
                dialogAlert2.showAndWait();
                controlador.estadoDeDientes();
                Stage stage1 = (Stage) nombreDiente.getScene().getWindow();
                stage1.close();
            }catch(HeadlessException | SQLException e){
                acceso=false;
                Logger.getLogger(Loginv2Controller.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }
    
    private void actualizar(String choice, String fecha, String description){
        String sql =  "UPDATE historial " +
                    "SET diaProcedimiento = ?," +
                    "estadoDiente = ?," +
                    "descripcion = ?" +
                    "WHERE idHistorial = "+id+"";
         try
        {
            PreparedStatement pst = cn.prepareStatement(sql);// con esta sentencia se insertan los datos en la base de datos
            pst.setString(1, fecha);
            pst.setString(2, choice);
            pst.setString(3, description);
            pst.executeUpdate();
            }
        catch (HeadlessException | SQLException ex){
            Logger.getLogger(MenuDientev2Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean existeTratamiento(int idP, int idD){
        id=0;
        String sql = "SELECT * FROM historial WHERE idPaciente = '"+idP+"' and idDiente = '"+idD+"'";
        try {
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql); 
                while(rs.next()){
                    id=rs.getInt(1);
                    descripcion.setText(rs.getString(6));
                }
            } catch (SQLException e) {
                Logger.getLogger(Loginv2Controller.class.getName()).log(Level.SEVERE, null, e);
            }
        acreditar = id >= 1;
        return acreditar;
    }
    
    private void incremento(){
        try {
            String sql = "select max(idHistorial) from historial";
            idTratamiento = 0;
            Statement st = cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                idTratamiento = rs.getInt(1)+1;
            }
        } catch (SQLException ex) {
            Logger.getLogger(MenuDientev2Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void setControllerConsulta(Consultav2Controller controller){
        controlador = controller;
    }
}
