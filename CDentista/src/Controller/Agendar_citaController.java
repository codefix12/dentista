/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import ConexionDB.Citas;
import ConexionDB.Conectar;
import ConexionDB.Conection;
import ConexionDB.Medicos;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author MrKim
 */
public class Agendar_citaController implements Initializable {

    @FXML    private AnchorPane Anchor;
    @FXML    private DatePicker fecha;
    @FXML    private TextField asunto;
    @FXML    private JFXTextField idPaciente;
    @FXML    private ComboBox<Medicos> idMedicos;
    @FXML    private TableView<Citas> tblCitas;
    @FXML    private TableColumn<Citas, Number> clmidPaciente;
    @FXML    private TableColumn<Citas, Number> clmidMedico;
    @FXML    private TableColumn<Citas, String> clmnombre;
    @FXML    private TableColumn<Citas, String> clmapellidoP;
    @FXML    private TableColumn<Citas, String> clmapellidoM;
    @FXML    private TableColumn<Citas, String> clmfecha;
    
    private ObservableList<Citas> citas;    
    private ObservableList<Medicos> listaMedicos;    
    private Conection conexion;
     Conectar cc= new Conectar();
    Connection cn= cc.conexion();
    @FXML
    private JFXButton btnAgendar;
    @FXML
    private TableColumn<Citas, Number> clmidCita;
    @FXML
    private JFXButton btnActualizar;
    @FXML
    private JFXTextField id_citas;
    @FXML
    private TableColumn<Citas, String> clmAsunto;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        conexion = new Conection();
	conexion.establecerConexion();
        
        listaMedicos = FXCollections.observableArrayList();
        citas = FXCollections.observableArrayList();
        
        Citas.llenarInformacionAlumnos(conexion.getConnection(), citas);
        Medicos.llenarInformacion(conexion.getConnection(), listaMedicos);
        //.llenarInformacionAlumnos(conexion.getConnection(), agenda, nombreBusqueda.getText(), apellidoPBusqueda.getText(), apellidoMBusqueda.getText());
        
        tblCitas.setItems(citas);
        idMedicos.setItems(listaMedicos);
        
        clmidCita.setCellValueFactory(new PropertyValueFactory<Citas,Number>("id"));
        clmidPaciente.setCellValueFactory(new PropertyValueFactory<Citas,Number>("idPaciente"));
        clmidMedico.setCellValueFactory(new PropertyValueFactory<Citas,Number>("idMedico"));
        clmnombre.setCellValueFactory(new PropertyValueFactory<Citas, String>("nombre"));
        clmapellidoP.setCellValueFactory(new PropertyValueFactory<Citas, String>("apellidoP"));
        clmapellidoM.setCellValueFactory(new PropertyValueFactory<Citas, String>("apellidoM"));
        clmfecha.setCellValueFactory(new PropertyValueFactory<Citas, String>("fecha"));
        clmAsunto.setCellValueFactory(new PropertyValueFactory<Citas, String>("asunto"));
        
        conexion.cerrarConexion();
        gestionarEventos();
    }    

    @FXML
    private void AgendarCita(ActionEvent event) {
        String sql;
        if(idPaciente.getText().equals("") || idMedicos.getValue().equals("") || fecha.getValue().equals("")
                || asunto.getText().equals("") ){
            Alert dialogAlert2 = new Alert(Alert.AlertType.WARNING);
            dialogAlert2.setTitle("Advertencia");
            dialogAlert2.setContentText("Hay Campos Vacios");
            dialogAlert2.initStyle(StageStyle.UTILITY);
            dialogAlert2.showAndWait();
            
        }else{
            sql="INSERT INTO citas (idMedico,idpaciente,Asunto, fecha) VALUES (?,?,?,?)";
            //String ins="INSERT INTO usuario (Login, Password,tipousuario) VALUES(?,?,?)";
            try {                                                             
                PreparedStatement pst = cn.prepareStatement(sql);
                pst.setInt(1, idMedicos.getValue().getIdMedico());
                pst.setInt(2, Integer.parseInt(idPaciente.getText()));
                pst.setString(3, asunto.getText());
                pst.setString(4, fecha.getValue().toString());
                int n = pst.executeUpdate();
                if(n> 0){
                    Alert dialogAlert2 = new Alert(Alert.AlertType.WARNING);
                    dialogAlert2.setTitle("Advertencia");
                    dialogAlert2.setContentText("Se guardo con exito");
                    dialogAlert2.initStyle(StageStyle.UTILITY);
                    dialogAlert2.showAndWait();
                    idPaciente.setText("");
                    asunto.setText("");
                    fecha.setValue(null);
                }
                
                conexion = new Conection();
                conexion.establecerConexion();

                listaMedicos = FXCollections.observableArrayList();
                citas = FXCollections.observableArrayList();

                Medicos.llenarInformacion(conexion.getConnection(), listaMedicos);
                Citas.llenarInformacionAlumnos(conexion.getConnection(), citas);

                tblCitas.setItems(citas);
                idMedicos.setItems(listaMedicos);
                clmidCita.setCellValueFactory(new PropertyValueFactory<Citas,Number>("id"));
                clmidPaciente.setCellValueFactory(new PropertyValueFactory<Citas,Number>("idPaciente"));        
                clmidMedico.setCellValueFactory(new PropertyValueFactory<Citas,Number>("idMedico"));
                clmnombre.setCellValueFactory(new PropertyValueFactory<Citas, String>("nombre"));
                clmapellidoP.setCellValueFactory(new PropertyValueFactory<Citas, String>("apellidoP"));
                clmapellidoM.setCellValueFactory(new PropertyValueFactory<Citas, String>("apellidoM"));
                clmfecha.setCellValueFactory(new PropertyValueFactory<Citas, String>("fecha"));
                clmAsunto.setCellValueFactory(new PropertyValueFactory<Citas, String>("asunto"));

                conexion.cerrarConexion();
                
            } catch (SQLException ex) {
                //System.out.println(ex.getMessage());
                Logger.getLogger(Agendar_citaController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }

    @FXML
    private void ActualizarCita(ActionEvent event) {
         String sql ="";
        if(idPaciente.getText().equals("") || idMedicos.getValue().equals("") || fecha.getValue().equals("")
                || asunto.getText().equals("")){
        }else{           
            sql="UPDATE `bookmedic`.`citas` "
                    + "SET `idMedico`='"+idMedicos.getValue().getIdMedico()+"', `"
                    + "idpaciente`='"+Integer.parseInt(idPaciente.getText())+"', "
                    + "`Asunto`='"+asunto.getText()+"', "
                    + "`fecha`='"+fecha.getValue().toString()+"' "
                    + "WHERE `idcitas`='"+Integer.parseInt(id_citas.getText())+"'";                       
            try {
                PreparedStatement pst = cn.prepareStatement(sql);
                /*pst.setInt(1, idMedicos.getValue().getIdMedico());
                pst.setInt(2, Integer.parseInt(idPaciente.getText()));
                pst.setString(3, asunto.getText());
                pst.setString(4, fecha.getValue().toString());
                pst.setInt(5, Integer.parseInt(id_citas.getText()));*/
                pst.executeUpdate();
                Alert dialogAlert2 = new Alert(Alert.AlertType.WARNING);
                    dialogAlert2.setTitle("Advertencia");
                    dialogAlert2.setContentText("Se actualizo con exito");
                    dialogAlert2.initStyle(StageStyle.UTILITY);
                    dialogAlert2.showAndWait();
                            
                            conexion = new Conection();
                            conexion.establecerConexion();
        
                            listaMedicos = FXCollections.observableArrayList();
                            citas = FXCollections.observableArrayList();

                            Medicos.llenarInformacion(conexion.getConnection(), listaMedicos);
                            Citas.llenarInformacionAlumnos(conexion.getConnection(), citas);

                            tblCitas.setItems(citas);
                            idMedicos.setItems(listaMedicos);
                            clmidCita.setCellValueFactory(new PropertyValueFactory<Citas,Number>("id"));
                            clmidPaciente.setCellValueFactory(new PropertyValueFactory<Citas,Number>("idPaciente"));        
                            clmidMedico.setCellValueFactory(new PropertyValueFactory<Citas,Number>("idMedico"));
                            clmnombre.setCellValueFactory(new PropertyValueFactory<Citas, String>("nombre"));
                            clmapellidoP.setCellValueFactory(new PropertyValueFactory<Citas, String>("apellidoP"));
                            clmapellidoM.setCellValueFactory(new PropertyValueFactory<Citas, String>("apellidoM"));
                            clmfecha.setCellValueFactory(new PropertyValueFactory<Citas, String>("fecha"));
                            clmAsunto.setCellValueFactory(new PropertyValueFactory<Citas, String>("asunto"));

                            conexion.cerrarConexion();
                            idPaciente.setText("");
                    asunto.setText("");
                    fecha.setValue(null);
                     btnAgendar.setDisable(false);
                     btnActualizar.setDisable(true);
                    
                            
            } catch (SQLException ex) {
                Logger.getLogger(Agendar_citaController.class.getName()).log(Level.SEVERE, null, ex);
            }            
        }
    }
    
    /**
    * Envia a los datos que estan la tabla de la fila seleccionada a los campos de texto  para actualizar los datos
    * @see
    */
    public void gestionarEventos(){
        tblCitas.getSelectionModel().selectedItemProperty().addListener(                
                new ChangeListener<Citas>() {
            @Override
            public void changed(ObservableValue<? extends Citas> observable, Citas oldValue, Citas newValue) {
                if(newValue != null){
                    idPaciente.setText(String.valueOf(newValue.getIdPaciente()));                       
                    id_citas.setText(String.valueOf(newValue.getId()));    
                    btnAgendar.setDisable(true);
                     btnActualizar.setDisable(false);
                }
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        }
    }
    

