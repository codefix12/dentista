/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import ConexionDB.Agenda;
import ConexionDB.Conection;
import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author SYSTEM
 */
public class InicioController implements Initializable {

    @FXML
    private AnchorPane vista;
    @FXML
    private AnchorPane inicio;
    @FXML
    private JFXButton consulta;
    @FXML
    private JFXButton informacion;
    @FXML
    private JFXButton agenda;
    @FXML
    private JFXButton ayuda;
    @FXML
    private JFXButton historial;
    final Tooltip tooltip = new Tooltip();
    final Tooltip tooltip1 = new Tooltip();
    final Tooltip tooltip2 = new Tooltip();
    final Tooltip tooltip3 = new Tooltip();
    final Tooltip tooltip4 = new Tooltip();
    @FXML
    private TableView<Agenda> tblAgenda;
    @FXML
    private TableColumn<Agenda, Number> id_Paciente;
    @FXML
    private TableColumn<Agenda,String> nombre;
    @FXML
    private TableColumn<Agenda,String> apellidoP;
    @FXML
    private TableColumn<Agenda, String> apellidoM;
    @FXML
    private TableColumn<Agenda, String> fecha;
    @FXML
    private TableColumn<Agenda, String> hora;
    private ObservableList<Agenda> agenda1;    
    private Conection conexion;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        tooltip.setText("Iniciar Consulta");
        consulta.setTooltip(tooltip);
        tooltip1.setText("Acerca de ...");
        informacion.setTooltip(tooltip1);
        tooltip2.setText("Agenda");
        agenda.setTooltip(tooltip2);
        tooltip3.setText("Ayuda");
        ayuda.setTooltip(tooltip3);
        tooltip4.setText("Historial");
        historial.setTooltip(tooltip4);
        conexion = new Conection();
	conexion.establecerConexion();
        
        agenda1 = FXCollections.observableArrayList();
        Agenda.llenarInformacionAlumnos1(conexion.getConnection(), agenda1);
        //Agenda.llenarInformacionAlumnos(conexion.getConnection(), agenda1);
        tblAgenda.setItems(agenda1);
        
        id_Paciente.setCellValueFactory(new PropertyValueFactory<Agenda,Number>("id"));
        nombre.setCellValueFactory(new PropertyValueFactory<Agenda, String>("nombre"));
        apellidoP.setCellValueFactory(new PropertyValueFactory<Agenda, String>("apellidoP"));
        apellidoM.setCellValueFactory(new PropertyValueFactory<Agenda, String>("apellidoM"));
        fecha.setCellValueFactory(new PropertyValueFactory<Agenda, String>("fecha"));
        hora.setCellValueFactory(new PropertyValueFactory<Agenda, String>("hora"));
        
        conexion.cerrarConexion();
    }    
    
    
    
    @FXML
    private void consultaInicio(ActionEvent event) throws IOException {
        inicio.toBack();
        Pane ini = FXMLLoader.load(getClass().getResource("/FileFXML/Consultav2.fxml"));
        vista.getChildren().setAll(ini);
    }

    @FXML
    private void informacionInicio(ActionEvent event) throws IOException {
        inicio.toBack();
        Pane ini = FXMLLoader.load(getClass().getResource ("/FileFXML/About.fxml"));
        vista.getChildren().setAll(ini);
    }

    @FXML
    private void agendaInicio(ActionEvent event) throws IOException {
        inicio.toBack();
        Pane ini = FXMLLoader.load(getClass().getResource("/FileFXML/Agendav3.fxml"));
        vista.getChildren().setAll(ini);
    }

    @FXML
    private void ayudaInicio(ActionEvent event) throws IOException {
        inicio.toBack();
        Pane ini = FXMLLoader.load(getClass().getResource ("/FileFXML/Ayudav2.fxml"));
        vista.getChildren().setAll(ini);
    }
    
    @FXML
    private void historiaInicio(ActionEvent event) throws IOException {
        inicio.toBack();
        Pane ini = FXMLLoader.load(getClass().getResource ("/FileFXML/Historial.fxml"));
        vista.getChildren().setAll(ini);
    }
}
