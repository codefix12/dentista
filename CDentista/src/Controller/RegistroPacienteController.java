/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import ConexionDB.Conexion;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.stage.StageStyle;


/**
 * FXML Controller class
 *
 * @author SYSTEM
 */
public class RegistroPacienteController implements Initializable {

    
    private int idMedico;
    private int idPaciente;
    private Conexion cc = new Conexion();
    private Connection cn= cc.conexion();
    @FXML
    private JFXTextField nombre;
    @FXML
    private JFXTextField apellidoP;
    @FXML
    private JFXTextField apellidoM;
    @FXML
    private JFXTextField edad;
    @FXML
    private JFXTextField calle;
    @FXML
    private JFXTextField colonia;
    @FXML
    private JFXTextField ciudad;
    @FXML
    private JFXTextField telefono;
    @FXML
    private JFXTextField correo;
    @FXML
    private JFXDatePicker nacimiento;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        incremento();
    }    

    @FXML
    private void cancelRegistro(ActionEvent event) {
    }

    @FXML
    private void registroPaciente(ActionEvent event) {
        String day = nacimiento.getValue().toString();
        if(nombre.getText().equals("") || apellidoP.getText().equals("") || apellidoM.getText().equals("")
                || edad.getText().equals("") || calle.getText().equals("") || colonia.getText().equals("") 
                || ciudad.getText().equals("") || day == null ){
            Alert dialogAlert2 = new Alert(Alert.AlertType.INFORMATION);
            dialogAlert2.setTitle("Advertencia");
            dialogAlert2.setContentText("Algun campo no esta lleno");
            dialogAlert2.initStyle(StageStyle.UTILITY);
            dialogAlert2.showAndWait();
        }else{
            String sql = "INSERT INTO paciente(idpaciente, nombre, apellido_paterno, apellido_materno, fechaNacimiento, calle, colonia, ciudad, telefono, email, idMedico) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
            try {
                PreparedStatement pst = cn.prepareStatement(sql);
                pst.setInt(1, idPaciente);
                pst.setString(2, nombre.getText());
                pst.setString(3, apellidoP.getText());
                pst.setString(4, apellidoM.getText());
                pst.setString(5, day);
                pst.setString(6, calle.getText());
                pst.setString(7, colonia.getText());
                pst.setString(8, ciudad.getText());
                pst.setString(9, telefono.getText());
                pst.setString(10, correo.getText());
                pst.setInt(11, idMedico);
                int n = pst.executeUpdate();
                if(n>0){
                     Alert dialogAlert2 = new Alert(Alert.AlertType.INFORMATION);
                    dialogAlert2.setTitle("");
                    dialogAlert2.setContentText("Usuario Guardado Corecctamente");
                    dialogAlert2.initStyle(StageStyle.UTILITY);
                    dialogAlert2.showAndWait();
                    nombre.setText(null);
                    apellidoP.setText(null);
                    apellidoM.setText(null);
                    calle.setText(null);
                    colonia.setText(null);
                    ciudad.setText(null);
                    telefono.setText(null);
                    correo.setText(null);
                }else{
                    Alert dialogAlert2 = new Alert(Alert.AlertType.WARNING);
                    dialogAlert2.setTitle("");
                    dialogAlert2.setContentText("Usuario ya existe");
                    dialogAlert2.initStyle(StageStyle.UTILITY);
                    dialogAlert2.showAndWait();
                }
            } catch (SQLException ex) {
                Logger.getLogger(RegistroPacienteController.class.getName()).log(Level.SEVERE, null, ex);

            }

        }
    }
    
    public void setIdMedico(int id){
        idMedico = id;
    }
    
    private void incremento(){
        try {
            String sql = "select max(idPaciente) from paciente";
            idPaciente = 0;
            Statement st = cn.createStatement();
            ResultSet rs=st.executeQuery(sql);
            while(rs.next()){
                idPaciente = rs.getInt(1)+1;
            }
        } catch (SQLException ex) {
            Logger.getLogger(MenuDientev2Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
