/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import ConexionDB.Conexion;
//import Controller.ConsultaController;
//import Controller.LoginController;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.StageStyle;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author SYSTEM
 */
public class Consultav2Controller implements Initializable {

    @FXML
    private AnchorPane uni;
    @FXML
    private AnchorPane consulta;
    @FXML
    private JFXTextField txtNombre;
    @FXML
    private JFXTextField txtEdad;
    @FXML
    private JFXTextField txtSexo;
    @FXML
    private JFXTextField txtIDpaciente;
    @FXML
    private JFXTextArea txtReceta;
    @FXML
    private JFXTextArea txtObservaciones;
    @FXML
    private TextField txtTipoDiente;
    @FXML
    private TextField txtPosicion;
    @FXML
    private JFXTextArea txtDetalles;
    @FXML
    private AnchorPane busqueda;
    @FXML
    private JFXTextField nombre;
    @FXML
    private JFXTextField apellidoM;
    @FXML
    private JFXTextField apellidoP;
    private int idDiente;
    private int id;
    private String nombreDB;
    private String apellidoPDB;
    private String apellidoMDB;
    private String edad;
    private String medico;
    Conexion cc = new Conexion();
    Connection cn=cc.conexion();
    @FXML
    private AnchorPane bloqueo;
    @FXML
    private Button diente1;
    @FXML
    private Button diente1f1;
    @FXML
    private Button diente3;
    @FXML
    private Button diente2;
    @FXML
    private Button diente1f2;
    @FXML
    private Button diente1f3;
    @FXML
    private Button diente2f1;
    @FXML
    private Button diente2f2;
    @FXML
    private Button diente2f3;
    @FXML
    private Button diente3f1;
    @FXML
    private Button diente3f2;
    @FXML
    private Button diente3f3;
    @FXML
    private Button diente8;
    @FXML
    private Button diente7;
    @FXML
    private Button diente6;
    @FXML
    private Button diente5;
    @FXML
    private Button diente4;
    @FXML
    private Button diente4f1;
    @FXML
    private Button diente4f2;
    @FXML
    private Button diente4f3;
    @FXML
    private Button diente5f1;
    @FXML
    private Button diente5f2;
    @FXML
    private Button diente5f3;
    @FXML
    private Button diente6f1;
    @FXML
    private Button diente6f2;
    @FXML
    private Button diente6f3;
    @FXML
    private Button diente7f1;
    @FXML
    private Button diente7f2;
    @FXML
    private Button diente7f3;
    @FXML
    private Button diente8f1;
    @FXML
    private Button diente8f2;
    @FXML
    private Button diente8f3;
    @FXML
    private AnchorPane salida;
    @FXML
    private AnchorPane principal;
    @FXML
    private Button diente9;
    @FXML
    private Button diente9f1;
    @FXML
    private Button diente9f2;
    @FXML
    private Button diente9f3;
    @FXML
    private Button diente10;
    @FXML
    private Button diente10f1;
    @FXML
    private Button diente10f2;
    @FXML
    private Button diente10f3;
    @FXML
    private Button diente11;
    @FXML
    private Button diente11f1;
    @FXML
    private Button diente11f2;
    @FXML
    private Button diente11f3;
    @FXML
    private Button diente12;
    @FXML
    private Button diente12f1;
    @FXML
    private Button diente12f2;
    @FXML
    private Button diente12f3;
    @FXML
    private Button diente13;
    @FXML
    private Button diente13f1;
    @FXML
    private Button diente13f2;
    @FXML
    private Button diente13f3;
    @FXML
    private Button diente14;
    @FXML
    private Button diente14f1;
    @FXML
    private Button diente14f2;
    @FXML
    private Button diente14f3;
    @FXML
    private Button diente15;
    @FXML
    private Button diente15f1;
    @FXML
    private Button diente15f2;
    @FXML
    private Button diente15f3;
    @FXML
    private Button diente16;
    @FXML
    private Button diente16f1;
    @FXML
    private Button diente16f2;
    @FXML
    private Button diente16f3;
    @FXML
    private Button diente24;
    @FXML
    private Button diente24f1;
    @FXML
    private Button diente24f2;
    @FXML
    private Button diente24f3;
    @FXML
    private Button diente23;
    @FXML
    private Button diente23f1;
    @FXML
    private Button diente23f2;
    @FXML
    private Button diente23f3;
    @FXML
    private Button diente22;
    @FXML
    private Button diente22f1;
    @FXML
    private Button diente22f2;
    @FXML
    private Button diente22f3;
    @FXML
    private Button diente21;
    @FXML
    private Button diente21f1;
    @FXML
    private Button diente21f2;
    @FXML
    private Button diente21f3;
    @FXML
    private Button diente20;
    @FXML
    private Button diente20f1;
    @FXML
    private Button diente20f2;
    @FXML
    private Button diente20f3;
    @FXML
    private Button diente19;
    @FXML
    private Button diente19f1;
    @FXML
    private Button diente19f2;
    @FXML
    private Button diente19f3;
    @FXML
    private Button diente18;
    @FXML
    private Button diente18f1;
    @FXML
    private Button diente18f2;
    @FXML
    private Button diente18f3;
    @FXML
    private Button diente17;
    @FXML
    private Button diente17f1;
    @FXML
    private Button diente17f2;
    @FXML
    private Button diente17f3;
    @FXML
    private Button diente25;
    @FXML
    private Button diente25f1;
    @FXML
    private Button diente25f2;
    @FXML
    private Button diente25f3;
    @FXML
    private Button diente26;
    @FXML
    private Button diente26f1;
    @FXML
    private Button diente26f2;
    @FXML
    private Button diente26f3;
    @FXML
    private Button diente27;
    @FXML
    private Button diente27f1;
    @FXML
    private Button diente27f2;
    @FXML
    private Button diente27f3;
    @FXML
    private Button diente28;
    @FXML
    private Button diente28f1;
    @FXML
    private Button diente28f2;
    @FXML
    private Button diente28f3;
    @FXML
    private Button diente29;
    @FXML
    private Button diente29f1;
    @FXML
    private Button diente29f2;
    @FXML
    private Button diente29f3;
    @FXML
    private Button diente30;
    @FXML
    private Button diente30f1;
    @FXML
    private Button diente30f2;
    @FXML
    private Button diente30f3;
    @FXML
    private Button diente31;
    @FXML
    private Button diente31f1;
    @FXML
    private Button diente31f2;
    @FXML
    private Button diente31f3;
    @FXML
    private Button diente32;
    @FXML
    private Button diente32f1;
    @FXML
    private Button diente32f2;
    @FXML
    private Button diente32f3;
    private String estadoD = null;
    private Consultav2Controller controlador;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
    }    
    
    public void setData(String r){
        medico = r;
    }

    @FXML
    private void buttonImprimir(ActionEvent event) {
        try {
            this.impresion(txtNombre.getText());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Consultav2Controller.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(Consultav2Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void impresion(String nombre) throws FileNotFoundException, DocumentException{
        if(txtNombre.getText().equals("")|| txtReceta.getText().equals("") || txtEdad.getText().equals("") || 
                txtSexo.getText().equals("") || txtObservaciones.getText().equals("") || 
                 txtIDpaciente.equals("")){
            Alert dialogAlert2 = new Alert(Alert.AlertType.WARNING);
                dialogAlert2.setTitle("Campos vacios");
                dialogAlert2.setContentText("Hay algun campo vacio");
                dialogAlert2.initStyle(StageStyle.UTILITY);
                dialogAlert2.showAndWait();
        }else{
            String fech;
            FileOutputStream archivo = new FileOutputStream(new File("C:\\Recetas\\"+ nombre+Fecha() +".pdf"));
            Document docto = new Document();        
            PdfWriter.getInstance(docto, archivo);
            docto.open();
            Paragraph parrafo = new Paragraph("-------Receta Medica-------");
            parrafo.setAlignment(1);
            
            docto.add(parrafo);                                       
            docto.add(new Paragraph("\n                                         Nombre del Paciente: " + txtNombre.getText()));   
            docto.add(new Paragraph("\n*Medicamentos "));
            docto.add(new Paragraph(txtReceta.getText()));
            docto.add(new Paragraph("\n\n*Observaciones"));
            docto.add(new Paragraph(txtObservaciones.getText()));
            //docto.add(new Paragraph("Diente Removido"));
            docto.add(new Paragraph("\n\n\n\n\n_______________"));
            docto.add(new Paragraph("   firma."));
            docto.close();
            Alert dialogAlert2 = new Alert(Alert.AlertType.INFORMATION);
            dialogAlert2.setTitle("Pdf");
            dialogAlert2.setContentText("El Pdf se genero con exito");
            dialogAlert2.initStyle(StageStyle.UTILITY);
            dialogAlert2.showAndWait();
                //System.out.println(Fecha());
            this.abrir(nombre);
        }
    }      
    
     public static String Fecha(){
         Date fecha = new Date();
         SimpleDateFormat formato = new SimpleDateFormat("dd-MM-YYYY");
         return formato.format(fecha);                  
     }
     
     
     public void abrir(String nombre){                               
        try {
            File path = new File("C:\\Recetas\\"+ nombre+Fecha() +".pdf");
            Desktop.getDesktop().open(path);
        } catch (IOException ex) {
            Logger.getLogger(Consultav2Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
     }

    @FXML
    private void txtTerminarConsulta(ActionEvent event) throws IOException {
        Alert dialogAlert2 = new Alert(Alert.AlertType.CONFIRMATION);
        dialogAlert2.setTitle("Confirmacion");
        dialogAlert2.setContentText("");
        dialogAlert2.initStyle(StageStyle.UTILITY);
        dialogAlert2.showAndWait();
        String s = dialogAlert2.getResult().getText();
        //System.out.println(s);
        if(s.equals("Aceptar")){
            principal.toBack();
            Pane ini = FXMLLoader.load(getClass().getResource("/FileFXML/Consultav2.fxml"));
            salida.getChildren().setAll(ini);
        }
        
    }

    @FXML
    private void ButtonCancelar(ActionEvent event) {
    }

    @FXML
    private void busquedaClick(ActionEvent event) {
        if(nombre.getText().equals(null) || apellidoP.getText().equals(null) || apellidoM.getText().equals(null) ||
            nombre.getText().equals("") || apellidoP.getText().equals("") || apellidoM.getText().equals("")){
            Alert dialogAlert2 = new Alert(Alert.AlertType.WARNING);
            dialogAlert2.setTitle("Advertencia");
            dialogAlert2.setContentText("Hay Campos Vacios");
            dialogAlert2.initStyle(StageStyle.UTILITY);
            dialogAlert2.showAndWait();
        }else{
            if(buscar()){
                bloqueo.toBack();
                txtNombre.setText(nombreDB + " " + apellidoPDB + " " + apellidoMDB /*nombreDB+ " " + apellidoPDB + " " + apellidoMDB*/);
                txtEdad.setText(edad);
                txtIDpaciente.setText(String.valueOf(id));
                txtSexo.setText("Hombre");
                busqueda.toBack();
                estadoDeDientes();
            }else{
                Alert dialogAlert2 = new Alert(Alert.AlertType.WARNING);
                dialogAlert2.setTitle("Advertencia");
                dialogAlert2.setContentText("No encontrado");
                dialogAlert2.initStyle(StageStyle.UTILITY);
                dialogAlert2.showAndWait();
            }
            /**/
        }
    }
    boolean acreditar;
    
    /**
    * Realiza busqueda del paciente en la base de datos y retorna una acreditacion 
    * @return acreditar 
    */
    private boolean buscar(){
        id=-1;
        String bq = nombre.getText();
        String bq1 = apellidoP.getText();
        String bq3 = apellidoM.getText();
        String sql = "SELECT * FROM paciente WHERE nombre = '"+ bq +"' and apellido_paterno = '"+bq1+"' and apellido_materno = '"+bq3+"'";
        try {
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql); 
                while(rs.next()){
                    id=rs.getInt(1);
                    nombreDB = rs.getString(2);
                    apellidoPDB = rs.getString(3);
                    apellidoMDB = rs.getString(4);
                    edad = rs.getString(5);
                }
                //System.out.println(id);
                //acreditar = true;
            } catch (SQLException e) {
                Logger.getLogger(Loginv2Controller.class.getName()).log(Level.SEVERE, null, e);
                //acreditar = false;
            }
        
        acreditar = id > -1;

        return acreditar;
    }

    @FXML
    private void actualizar(ActionEvent event) {
        estadoDeDientes();
    }

    @FXML
    private void bloqueo(MouseEvent event) {
        Alert dialogAlert2 = new Alert(Alert.AlertType.WARNING);
        dialogAlert2.setTitle("Advertencia");
        dialogAlert2.setContentText("Primero realiza una busqueda");
        dialogAlert2.initStyle(StageStyle.UTILITY);
        dialogAlert2.showAndWait();
    }
    
    /**
    * Inicia el menu de carga de dientes
    * @param dato nombre del diente
    * @param id numero de id del paciente
    */
    private void aperturaMenu(String dato, int idDiente) throws IOException{
        Stage stage = new Stage();
        FXMLLoader loader= new FXMLLoader();
        Parent root = loader.load(getClass().getResource("/FileFXML/MenuDientev2.fxml").openStream());
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Detalles");
        MenuDientev2Controller d = (MenuDientev2Controller)loader.getController();
        d.setDiente(dato, idDiente,id);  
        d.setIDPaciente(id);
        d.setControllerConsulta(controlador);
        stage.getIcons().add(new Image(Consultav2Controller.class.getResourceAsStream( "/NCSS/Login.png" ))); 
        stage.show();
        
        //d.existeTratamiento();
    }

    @FXML
    private void diente1(ActionEvent event) throws IOException {
        idDiente = 1;
        txtTipoDiente.setText("Tercer Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente2(ActionEvent event) throws IOException {
        idDiente = 2;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente3(ActionEvent event) throws IOException {
        idDiente = 3;
        txtTipoDiente.setText("Primer Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }
    
    public void estadoDeDientes(){
        if(busquedaTratamiento(1)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente1.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente1.setVisible(false);
                diente1f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente1.setVisible(false);
                diente1f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente1.setVisible(false);
                diente1f2.setVisible(true);
            }  
            estadoD = null;
        }
        
        if(busquedaTratamiento(2)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente2.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente2.setVisible(false);
                diente2f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente2.setVisible(false);
                diente2f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente2.setVisible(false);
                diente2f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(3)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente3.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente3.setVisible(false);
                diente3f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente3.setVisible(false);
                diente3f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente3.setVisible(false);
                diente3f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(4)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente4.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente4.setVisible(false);
                diente4f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente4.setVisible(false);
                diente4f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente4.setVisible(false);
                diente4f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(5)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente5.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente5.setVisible(false);
                diente5f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente5.setVisible(false);
                diente5f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente5.setVisible(false);
                diente5f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(6)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente6.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente6.setVisible(false);
                diente6f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente6.setVisible(false);
                diente6f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente6.setVisible(false);
                diente6f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(7)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente7.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente7.setVisible(false);
                diente7f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente7.setVisible(false);
                diente7f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente7.setVisible(false);
                diente7f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(8)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente8.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente8.setVisible(false);
                diente8f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente8.setVisible(false);
                diente8f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente8.setVisible(false);
                diente8f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(9)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente9.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente9.setVisible(false);
                diente9f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente9.setVisible(false);
                diente9f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente9.setVisible(false);
                diente9f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(10)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente10.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente10.setVisible(false);
                diente10f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente10.setVisible(false);
                diente10f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente10.setVisible(false);
                diente10f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(11)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente11.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente11.setVisible(false);
                diente11f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente11.setVisible(false);
                diente11f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente11.setVisible(false);
                diente11f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(12)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente12.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente12.setVisible(false);
                diente12f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente12.setVisible(false);
                diente12f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente12.setVisible(false);
                diente12f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(13)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente13.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente13.setVisible(false);
                diente13f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente13.setVisible(false);
                diente13f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente13.setVisible(false);
                diente13f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(14)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente14.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente14.setVisible(false);
                diente14f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente14.setVisible(false);
                diente14f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente14.setVisible(false);
                diente14f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(15)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente15.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente15.setVisible(false);
                diente15f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente15.setVisible(false);
                diente15f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente15.setVisible(false);
                diente15f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(16)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente16.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente16.setVisible(false);
                diente16f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente16.setVisible(false);
                diente16f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente16.setVisible(false);
                diente16f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(17)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente17.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente17.setVisible(false);
                diente17f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente17.setVisible(false);
                diente17f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente17.setVisible(false);
                diente17f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(18)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente18.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente18.setVisible(false);
                diente18f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente18.setVisible(false);
                diente18f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente18.setVisible(false);
                diente18f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(19)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente19.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente19.setVisible(false);
                diente19f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente19.setVisible(false);
                diente19f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente19.setVisible(false);
                diente19f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(20)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente20.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente20.setVisible(false);
                diente20f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente20.setVisible(false);
                diente20f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente20.setVisible(false);
                diente20f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(21)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente21.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente21.setVisible(false);
                diente21f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente21.setVisible(false);
                diente21f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente21.setVisible(false);
                diente21f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(22)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente22.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente22.setVisible(false);
                diente22f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente22.setVisible(false);
                diente22f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente22.setVisible(false);
                diente22f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(23)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente23.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente23.setVisible(false);
                diente23f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente23.setVisible(false);
                diente23f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente23.setVisible(false);
                diente23f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(24)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente24.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente24.setVisible(false);
                diente24f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente24.setVisible(false);
                diente24f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente24.setVisible(false);
                diente24f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(25)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente25.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente25.setVisible(false);
                diente25f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente25.setVisible(false);
                diente25f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente25.setVisible(false);
                diente25f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(26)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente26.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente26.setVisible(false);
                diente26f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente26.setVisible(false);
                diente26f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente26.setVisible(false);
                diente26f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(27)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente27.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente27.setVisible(false);
                diente27f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente27.setVisible(false);
                diente27f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente27.setVisible(false);
                diente27f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(28)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente28.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente28.setVisible(false);
                diente28f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente28.setVisible(false);
                diente28f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente28.setVisible(false);
                diente28f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(29)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente29.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente29.setVisible(false);
                diente29f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente29.setVisible(false);
                diente29f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente29.setVisible(false);
                diente29f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(30)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente30.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente30.setVisible(false);
                diente30f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente30.setVisible(false);
                diente30f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente30.setVisible(false);
                diente30f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(31)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente31.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente31.setVisible(false);
                diente31f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente31.setVisible(false);
                diente31f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente31.setVisible(false);
                diente31f2.setVisible(true);
            }
            estadoD = null;
        }
        
        if(busquedaTratamiento(32)){
            if(estadoD == null || estadoD.equals("Normal")){
                diente32.setVisible(true);
            }
            else if(estadoD.equals("En tratamiento") && estadoD != null){//
                diente32.setVisible(false);
                diente32f3.setVisible(true);
            }
            else if(estadoD.equals("Inexistente") && estadoD != null){
                diente32.setVisible(false);
                diente32f1.setVisible(true);
            }
            else if(estadoD.equals("Extraido") && estadoD != null){
                diente32.setVisible(false);
                diente32f2.setVisible(true);
            }
            estadoD = null;
        }
    }
    
    
    /**
    *Busca si un tratamiento existe
    * @param idDiente nombre del diente
    * @return
    */
    private boolean busquedaTratamiento(int idDiente){
        String sql = "SELECT * FROM historial WHERE idPaciente = '"+ id +"' and idDiente = '"+ idDiente + "'";
        try {
                Statement st = cn.createStatement();
                ResultSet rs = st.executeQuery(sql); 
                while(rs.next()){
                    estadoD = rs.getString(5);
                }
                //System.out.println(id);
                acreditar = true;
            } catch (SQLException e) {
                Logger.getLogger(Loginv2Controller.class.getName()).log(Level.SEVERE, null, e);
                acreditar = false;
            }
        return acreditar;
    }

    @FXML
    private void diente8(ActionEvent event) throws IOException {
        idDiente = 8;
        txtTipoDiente.setText("Incisivo Central");
        String diente = txtTipoDiente.getText();
        txtPosicion.setText("Inferior Izquierda");
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente7(ActionEvent event) throws IOException {
        idDiente = 7;
        txtTipoDiente.setText("Incisivo Lateral");
        String diente = txtTipoDiente.getText();
        txtPosicion.setText("Inferior Izquierda");
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente6(ActionEvent event) throws IOException {
        idDiente = 6;
        txtTipoDiente.setText("Canino");
        String diente = txtTipoDiente.getText();
        txtPosicion.setText("Inferior Izquierda");
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente5(ActionEvent event) throws IOException {
        idDiente = 5;
        txtTipoDiente.setText("Primer Pre-Molar");
        String diente = txtTipoDiente.getText();
        txtPosicion.setText("Inferior Izquierda");
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente4(ActionEvent event) throws IOException {
        idDiente = 4;
        txtTipoDiente.setText("Segundo Pre-Molar");
        String diente = txtTipoDiente.getText();
        txtPosicion.setText("Inferior Izquierda");
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente9(ActionEvent event) throws IOException {
        idDiente = 9;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente10(ActionEvent event) throws IOException {
        idDiente = 10;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente11(ActionEvent event) throws IOException {
        idDiente = 11;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente12(ActionEvent event) throws IOException {
        idDiente = 12;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente13(ActionEvent event) throws IOException {
        idDiente = 13;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente14(ActionEvent event) throws IOException {
        idDiente = 14;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente15(ActionEvent event) throws IOException {
        idDiente = 15;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente16(ActionEvent event) throws IOException {
        idDiente = 16;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente24(ActionEvent event) throws IOException {
        idDiente = 24;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente23(ActionEvent event) throws IOException {
        idDiente = 23;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente22(ActionEvent event) throws IOException {
        idDiente = 22;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente21(ActionEvent event) throws IOException {
        idDiente = 21;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente20(ActionEvent event) throws IOException {
        idDiente = 20;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente19(ActionEvent event) throws IOException {
        idDiente = 19;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente18(ActionEvent event) throws IOException {
        idDiente = 18;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente17(ActionEvent event) throws IOException {
        idDiente = 17;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente25(ActionEvent event) throws IOException {
        idDiente = 25;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente26(ActionEvent event) throws IOException {
        idDiente = 26;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente27(ActionEvent event) throws IOException {
        idDiente = 27;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente28(ActionEvent event) throws IOException {
        idDiente = 28;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente29(ActionEvent event) throws IOException {
        idDiente = 29;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente30(ActionEvent event) throws IOException {
        idDiente = 30;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente31(ActionEvent event) throws IOException {
        idDiente = 31;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }

    @FXML
    private void diente32(ActionEvent event) throws IOException {
        idDiente = 32;
        txtTipoDiente.setText("Segundo Molar");
        txtPosicion.setText("Inferior Izquierda");
        String diente = txtTipoDiente.getText();
        //Apertura de ventana Registro
        aperturaMenu(diente, idDiente);
    }
    
    /**
    * Envia el controlador de Consulta para ocupar sus metodos
    * @param    controller nombre del diente
    * 
    */
    public void setController(Consultav2Controller controller){
        controlador = controller;
    }
}
